/**
 * Copyright 2020-2022 Openfintechlab, Inc. All rights reserved.
 * Licenses: LICENSE.md
 * Description:
 * Logger utility class
 */
import log4js       from "log4js";
const package_json = require('../../package.json');

log4js.configure({
    appenders: {
      out: {
        type: 'console',
        
        layout: {
          type: 'pattern',
          // Refer to: https://github.com/log4js-node/log4js-node/blob/master/docs/layouts.md
          pattern: '%[[%d]-[%p-(%l)]-[%h:%x{package_name}.%x{package_version}]:%] %m',
          tokens: {
            package_name: function () { return package_json.name; },
            package_version: function () { return package_json.version; },
          }
        }
      }
    },
    categories: {
      default: { appenders: ['out'], level: 'info', enableCallStack: true }
    }
  });
const logger = log4js.getLogger();
// Change to AppConfig.config.NNASA       
logger.level = process.env.OFL_MED_NODE_ENV || 'info' ;        

export default logger;