/**
 * Copyright 2020-2022 Openfintechlab, Inc. All rights reserved.
 * Licenses: LICENSE.md
 * Description:
 * Root enry level file forr bootstarting node js application
 */

import express                              from "express";
import KeycloakController                   from "../mapping/KeycloakLoginController";
import AppConfig, {AppHealth}                          from "../config/AppConfig";
import logger                               from "../utils/Logger";
import { v4 as uuidv4 }                     from "uuid";
import {PostRespBusinessObjects}            from "../mapping/bussObj/Response";
import util                                 from "util";
import LoginUserValidator                   from "../mapping/validators/LoginUserValidator";


const router: any = express.Router();

/**
 * Log's in a user by enquiring key-cloak
 */
router.post(`/login`, async (request:any, response:any) => {
    logger.debug(`Login API called`);    
    response.set("Content-Type","application/json; charset=utf-8");  
    let transid:string = uuidv4();      
    try{
        // Step#1: Validate request received from the client
        let businessObj = new LoginUserValidator(transid).validateLoginUser(request.body);        
        // Step#2: Call keyclaok and pass credentials so that token can be generated
        let resBO = await new KeycloakController(transid).getToken(businessObj.userDetails.username, businessObj.userDetails.password);
        response.status(200).send(resBO);
    }catch(error){
        logger.error(transid, `${util.inspect(error.message,{compact:true,colors:false, depth: null})}`);    
        logger.debug(transid, `${util.inspect(error.message,{compact:true,colors:false, depth: null})}`);    
        response.status(getErrRespCode(error));                
        if(error.metadata){            
            response.send(error);
        }else{                        
            response.send(new PostRespBusinessObjects.PostingResponse().generateBasicResponse("9901","Error while processing the request"));
        }     
    }
});

router.post(`/verify`, async(request:any, response:any)=> {
    logger.debug(`Verify user account`);
    response.set("Content-Type","application/json; charset=utf-8");  
    let transid:string = uuidv4();      
    try{
        // Step#1: Validate the request
        let businessObj = new LoginUserValidator(transid).validateToken(request.body);
        // Step#2: Authorize the user token
        let resBO = await new KeycloakController(transid).validateToken(businessObj.tokenDetail.access_token);
        response.status(200).send(resBO);
    }catch(error){
        logger.error(`${util.inspect(error,{compact:true,colors:false, depth: null})}`);    
        response.status(getErrRespCode(error));                
        if(error.metadata){            
            response.send(error);
        }else{                        
            response.send(new PostRespBusinessObjects.PostingResponse().generateBasicResponse("9901","Error while processing the request"));
        }     
    }
});

function getErrRespCode(error:any): number{
    let respCode:number = 500;
    if(!error.metadata){
        respCode = 500;
    }else if(error.metadata.status === '8602'){
        respCode= 400;
    }else if(error.metadata.status === '8605'){
        respCode= 403;
    }else if(error.metadata.status === '8603'){
        respCode= 401;
    }else{
        respCode = 500;
    }        
    return respCode;
}

export default router;