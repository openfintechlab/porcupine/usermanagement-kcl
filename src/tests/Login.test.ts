import util                     from "util";
import OracleDBInteractionUtil  from '../utils/OracleDBInteractionUtil';
import {Main}                   from "../index";
import AppConfig                from "../config/AppConfig";
import logger                   from "../utils/Logger";
import axios                    from "axios";
import chalk                    from "chalk";
import UTSetup                  from "./setup_express";
import * as _                   from "lodash";

describe('Login and Logout Use-Cases', () => {
    const post_message = require(`./POST.message.json`);    
    const package_json = require('../../package.json');
    const package_name = package_json.name;
    
    beforeAll(async () => {        
        try{
            logger.info(`Package Name: ${package_name}`)
            await Main.start();            
            await OracleDBInteractionUtil.initPoolConnectionsWithConfig();         
        }catch(error){
            logger.error(`Error received while Bootstarting the solution: ${error}`);
            logger.error(`Unable to proceed. Closing application`);                   
        }            
    });

    afterAll(async ()=> {
        // Delete specific record
        try{
            await OracleDBInteractionUtil.closePoolAndExit(); 
            await Main.expressApp.server.close();            
        }catch(error){
            if(error.response){
                logger.info(chalk.bgYellow(`${util.inspect(error.response.data,{compact:true,colors:true, depth: null})}`))
            }else{
                logger.error(chalk.red(`Error while cleaning specific record`))
            }           
            fail(error.message);
        }
        
    });

    test(`/login endpoint for the package: ${package_name} - Success Tet-Case`, async() =>{
        const url = UTSetup.getURL() + `/login`; // here url = http://{host}:{port}/usermanagement/login
        logger.info(`Calling URL: `, `${url}`);
        try{
            const response = await axios({
                "method": "post",
                "url": url,
                "timeout": 5 * 1000, // timeout in milliseconds
                "headers": {"Content-Type": "application/json"},
                "data": post_message
            });            
            const statusCode:number = response.status;
            expect(statusCode === 200 || statusCode === 201).toBeTruthy(); 
            expect(response.data.metadata.status).toBe(`0000`);
            expect(response.data.tokenDetails.access_token).toBeDefined();

        }catch(error){
            logger.error(chalk.red(`${util.inspect(error.response,{compact:true,colors:true, depth: null})}`));
            if(error.response){
                logger.info(chalk.bgYellow(`${util.inspect(error.response.data,{compact:true,colors:true, depth: null})}`))
                expect(error.response.status).not.toBe(500);
                fail(error.message);                
            }else{                
                fail(error.message)                
            }               
        }
    });

    test(`/login endpoint for the package: ${package_name} - In-valid User Test-Case`, async() =>{
        const url = UTSetup.getURL() + `/login`; // here url = http://{host}:{port}/usermanagement/login
        logger.info(`Calling URL: `, `${url}`);
        post_message.userDetails.username = 'should_not_exist';
        try{
            const response = await axios({
                "method": "post",
                "url": url,
                "timeout": 5 * 1000, // timeout in milliseconds
                "headers": {"Content-Type": "application/json"},
                "data": post_message
            });            

        }catch(error){            
            if(error.response){
                logger.info(chalk.bgYellow(`${util.inspect(error.response.data,{compact:true,colors:true, depth: null})}`))
                const statusCode:number = error.response.status;
                expect(statusCode === 401).toBeTruthy(); 
                expect(error.response.data.metadata.status).toBe(`8603`);
            }else{                
                logger.error(chalk.red(`${util.inspect(error.response,{compact:true,colors:true, depth: null})}`));            
                fail(error.message)                
            }               
        }
    });

    test(`/login endpoint for the package: ${package_name} - In-valid JSON request Test-Case`, async() =>{
        const url = UTSetup.getURL() + `/login`; // here url = http://{host}:{port}/usermanagement/login
        logger.info(`Calling URL: `, `${url}`);
        post_message.userDetails.username = undefined;
        try{
            const response = await axios({
                "method": "post",
                "url": url,
                "timeout": 5 * 1000, // timeout in milliseconds
                "headers": {"Content-Type": "application/json"},
                "data": post_message
            });            

        }catch(error){            
            if(error.response){
                logger.info(chalk.bgYellow(`${util.inspect(error.response.data,{compact:true,colors:true, depth: null})}`))
                const statusCode:number = error.response.status;
                expect(statusCode === 400).toBeTruthy(); 
                expect(error.response.data.metadata.status).toBe(`8602`);
            }else{    
                logger.error(chalk.red(`${util.inspect(error.response,{compact:true,colors:true, depth: null})}`));            
                fail(error.message)                
            }               
        }
    });
    
});