/**
 * Copyright 2020-2022 Openfintechlab, Inc. All rights reserved.
 * Licenses: LICENSE.md
 * Description:
 * Controller for managing users on keycloak
 */

import util                             from "util";
import { v4 as uuidv4 }                 from "uuid";
import  logger                          from "../utils/Logger";
import {PostRespBusinessObjects}        from "./bussObj/Response";
import axios                            from "axios";
import AppConfig                        from "../config/AppConfig";
import qs                               from "qs";
import jwt_decode                       from "jwt-decode";
import jwt                              from "jsonwebtoken";
import { any } from "joi";

export default class KeycloakController{
    

    constructor(private referenceID: string){}

    public async getToken(userID: string, password: string){
        logger.info(this.referenceID,`Getting security token for user:`, userID);
        
        try{
            // Step#1: create form data            
            const qsData = qs.stringify({
                "client_id": AppConfig.config.client_id,
                "grant_type": AppConfig.config.grant_type,
                "client_secret": AppConfig.config.client_secret,
                "scope": AppConfig.config.scope,
                "username": userID,
                "password": password
            });
            logger.debug(`Calling IDAM API for validating the user with configurations `, `URL:`, `${AppConfig.config[`keycloak.login.url`]}`, `TimeOut:`,`${AppConfig.config[`keycloak.login.timeout`]}`,`Data:`  ,`${qsData}`);            
            const response = await axios({
                method: 'post',
                timeout: AppConfig.config[`keycloak_login_timeout`],
                url: AppConfig.config[`keycloak_login_url`],
                data: qsData,
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            });
            
            logger.debug(this.referenceID,`Output`, `${util.inspect(response.data,{compact:true,colors:true, depth: null})}`)
            if(response.status >= 200 && response.status <= 300 ){
                let decodedJWT = this.decodeToken(response.data.access_token);
                logger.debug(this.referenceID, `${util.inspect(decodedJWT,{compact:true,colors:true, depth: null})}`)                       
                return await this.createBusinessObject(response.data, decodedJWT, response.data.access_token);                
            }else{
                throw new PostRespBusinessObjects.PostingResponse().generateBasicResponse("8601","Error received while authenticating user");
            }                

        }catch(error){
            if(error.response){
                logger.error(this.referenceID,`${util.inspect(error.response.data,{compact:true,colors:true, depth: null})}`, error.response.status);                    
                if(error.response.status >= 400 && error.response.status <= 500){                    
                    if(AppConfig.config.OFL_MED_NODE_ENV.toLowerCase() === 'debug'){
                        throw new PostRespBusinessObjects.PostingResponse().generateBasicResponse("8603","Invalid user",  
                                    new PostRespBusinessObjects.Trace("error_description", error.response.data.error_description));
                    }else{                                                
                        throw new PostRespBusinessObjects.PostingResponse().generateBasicResponse("8603","Invalid user");
                    }
                } 
            }
            throw error; // Throw exception in all other cases
        }
    }

    /**
     * Validates the token and session
     * @param {string} jwtToken JWT token to validate
     */
    public async validateToken(jwtToken: string){
        // TODO! Pass the JWT token to the keyloak server and see if an active session is available. If no active session found,
        //       invalidates the session
        // await jwt.verify(jwtToken, (err:any, decode:any) =>{
        //     if(err){
        //         logger.info(this.referenceID, `error while validating token`, `${util.inspect(err,{compact:true,colors:true, depth: null})}`);
        //         throw new PostRespBusinessObjects.PostingResponse().generateBasicResponse("8605","User Expires");
        //     }else{
        //         logger.info(this.referenceID, `Token validated`);
        //         logger.debug(this.referenceID,  `${util.inspect(decode,{compact:true,colors:true, depth: null})}`);
        //         return new PostRespBusinessObjects.PostingResponse().generateBasicResponse("0000","Success!");
        //     }
        // });
        logger.info(`${jwtToken}`);
        let deocded = jwt.verify(jwtToken, 
                                    'M5ILfUxlz_HmtzHz1EutsVJEglUmmxau9apf5ceI54Zq49Sn1wpcriwmjU4P1yeu3RFlZSSsY68oUz6b6s52Lws6P8sP8h_UKbApHO-1UBqwq06xIKrc53UGojjhfMTysWw0u3JceLWBuSagC3NgwuNYk3JfXfBn2JDTAV-nH1OWeakaRjiSVCWglHYvBu2sTWZJg7Z4vSDgmDs1n7ajulhT_GmI3uFhf_c27KsDTJdVKR4Phnpd6go9IYTv-GTbN2y_J4GG01h0qHiKPPzJgwO0-8s05bdbRZcFSrY-o-O9r-c9eijcexqlXboirx82_MMQ_3i5hmx6tNZJv7GtFg',
                                    { algorithms: ['RS256'] }

                                );
        logger.info(this.referenceID,  `${util.inspect(deocded,{compact:true,colors:true, depth: null})}`);
        throw new PostRespBusinessObjects.PostingResponse().generateBasicResponse("8605","User Expires");
    }

    private async createBusinessObject(klResponse:any, decodedJWT:any, accessToken: any){
     return new Promise<any>((resolve:any, _:any)=> {
        var bussObj:any = {
            metadata: {
                "status": '0000',
                "description": 'Success!',
                "responseTime": new Date(),                    
            },
            "tokenDetails": {
                "session_state": klResponse.session_state,
                "token_type": klResponse.token_type,
                "userID": decodedJWT.sub, // User ID sent to the client as a user_id
                "expiresInSecs": klResponse.expires_in, // Expires in seconds
                "access_token": accessToken
            }
        };
        resolve(bussObj);
     });
    }

    private decodeToken(jwt:any){
        try{
            return jwt_decode(jwt);
        }
        catch(Error){
            return null;
        }
    }


}