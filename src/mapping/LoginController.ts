/**
 * Copyright 2020-2022 Openfintechlab, Inc. All rights reserved.
 * Licenses: LICENSE.md
 * Description:
 * Controller for log-in a user
 */

 export default class LoginController{
    constructor(private referenceID: string){}
 }