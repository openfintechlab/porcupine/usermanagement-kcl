import Joi                              from "joi";
import logger                           from "../../utils/Logger"
import {PostRespBusinessObjects}        from "../bussObj/Response";
import AppConfig                        from "../../config/AppConfig";

export default class LoginUserValidator{
    LoginUserSchemaModel = Joi.object({
        "userDetails": {
            "username": Joi.string().min(3).max(128).required(),
            "password": Joi.string().min(3).max(128).required()
        }
    });

    TokenValidationModel = Joi.object({
        "tokenDetail": {
            "access_token": Joi.string().required()
        }
    })

    constructor(private referenceID: string){}

    public validateLoginUser(request:any){
        let {error,value} = this.LoginUserSchemaModel.validate(request, { presence: "required" } );
        if(error){
            logger.error(this.referenceID, `Error while validating the schema model`, error.message);
            this.getValidationError(error);
        }else{
            return value;
        }
    }

    /**
     * Validates if the provided token JSON is according to the schema
     * @param request Request JSON message
     */
    public validateToken(request:any){
        let {error,value} = this.TokenValidationModel.validate(request, { presence: "required" } );
        if(error){
            logger.error(this.referenceID, `Error while validating the schema model`, error.message);
            this.getValidationError(error);
        }else{
            return value;
        }
    }

    private getValidationError(error:any){
        if(AppConfig.config.OFL_MED_NODE_ENV === 'debug'){
            let trace:PostRespBusinessObjects.Trace = new PostRespBusinessObjects.Trace("schema-validation",error.message);
                throw new PostRespBusinessObjects.PostingResponse().generateBasicResponse("8602","Schema validation error", trace);   
        }else{
            throw new PostRespBusinessObjects.PostingResponse().generateBasicResponse("8602","Schema validation error");   
        }           
    }
}